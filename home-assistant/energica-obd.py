import time
import serial
from requests import post
import logging

# SOME CONFIG REQUIRED HERE
haUrl = "http://homeassistant.local:8123/api/services/mqtt/publish"
device = "/dev/rfcomm0"  # link bluetooth dongle with 'sudo rfcomm bind 0 AA:BB:CC:DD:EE:FF'
haToken = "HA_TOKEN"     # create one in Home Assistant > Your Profile > long term token
pollDelay = 60           # seconds between getting OBDII data from dongle
restartDelay = 60*5      # seconds to wait before restarting OBDII connection after failure
deviceBaudRate = 9600    # should be OK for elm327 chipset
deviceTimeout = 1        # timeout in seconds
enableMqtt = 1           # change to 1 to enable or 0 to disable MQTT push msg

headers = {
    "Authorization": "Bearer " + haToken,
    "content-type": "application/json",
}

# logging config
logging.basicConfig(filename='energica-debug.log', encoding='utf-8', level=logging.DEBUG, format='%(asctime)s %(message)s')

# helper to send single command and print response
def send(command):
  print("CONN - sending:", command)
  logging.debug("TX:%s", command)
  ser.write(command)
  response = ser.readline()
  print("CONN - received:", response)
  logging.debug("RX:%s", response)
  return response

# helper to remove all formatting
def stripFormat(response):
  return response.decode().replace(" ","")

# Initial OBDII data connection
def initObd2Connection():
  global initOk 
  initOk = False
  print("INIT - Reset OBDII connection for device", device)
  logging.debug("init OBDII dongle")

  global ser
  ser = serial.Serial(device, deviceBaudRate, timeout=deviceTimeout)

  send(b"ATWS\r")  # Rcvd "ATWS\r\r\rELM327 v1.5\r\r>"
  
  # have a delay here cause otherwise sometimes the initial commands are received too fast
  time.sleep(3)

  # initial setup
  send(b"ATI\r")   # Rcvd "ELM327 v1.5\r\r>"
  send(b"AT@1\r")  # Rcvd "OBDII to RS232 Interpreter\r\r>"
  send(b"ATE0\r")  # Rcvd "ATE0\rOK\r\r>"
  send(b"ATSP6\r") # Rcvd "OK\r\r>"
  send(b"ATAT1\r") # Rcvd "OK\r\r>"
  send(b"ATH1\r")  # Rcvd "OK\r\r>"
  send(b"ATL0\r")  # Rcvd "OK\r\r>"
  send(b"ATS0\r")  # Rcvd "OK\r\r>"

  # get ambient temp on init, standard OBDII PID is 0146
  # example response 7EF037F0133, we need _33
  response = stripFormat(send(b"0146\r"))
  pushData("energica/temp/ambient", int(response[9:11],16)-40, "°C")

  # get ODO (distance since codes cleared, ODO not supported)
  # example response 7E80441313576, we need _3576
  response = stripFormat(send(b"0131\r"))
  pushData("energica/odo", int(response[9:13],16), "km")

  # initial setup cont.
  send(b"ATCAF1\r")        # Rcvd "OK\r\r>"
  send(b"ATSH79B\r")       # Rcvd "OK\r\r>"
  send(b"ATFCSH79B\r")     # Rcvd "OK\r\r>"
  send(b"ATFCSD300000\r")  # Rcvd "OK\r\r>"
  send(b"ATFCSM1\r")       # Rcvd "OK\r\r>"
  send(b"ATSH7E7\r")       # Rcvd "OK\r\r>"
  send(b"ATFCSH7E7\r")     # Rcvd "OK\r\r>"
  send(b"ATCRA7EF\r")      # Rcvd "OK\r\r>"

  # request vin (not that this responds in multiple lines...)
  send(b"0902\r")

  # request more standard OBDII
  send(b"ATCAF1\r")    # Rcvd "OK\r\r>"
  send(b"ATSH7EF\r")   # Rcvd "OK\r\r>"
  send(b"ATCRA7EF\r")  # Rcvd "OK\r\r>"

  #send(b"ATE1\r")           # echo ON, just to show config
  #send(b"ATE0\r")           # echo OFF

  # energica specific values
  send(b"ATCAF0\r")    # Rcvd "OK\r\r>"
  send(b"ATSH7DF\r")   # Rcvd "OK\r\r>"
  send(b"ATCRA200\r")  # Rcvd "OK\r\r>"
  send(b"001\r")
  # Rcvd "200164764170C01FFFE\r\r>"

  initOk = True
  print("INIT - OBDII connection ready")

# helper to convert 2 complement signed
def two_complement(hexstr, bits):
  value = int(hexstr,16)
  if value &(1<<(bits-1)):
    value -= 1 << bits
  return value

# consider handling it after stripFormat 200 0B 4B 64 0C 0C 50 00 51 > 2000B4B640C0C500051
def handleStrippedResponse(response):
  print("UTIL - Handling valid response data", response)
  pushData("energica/soc", int(response[5:7],16), "%")
  pushData("energica/temp/battery", int(response[9:11],16), "°C")
  pushData("energica/voltage", int(response[11:15],16)/10, "V")
  pushData("energica/current", two_complement(response[15:19],16)/10, "A")

# helper to push data to MQTT
def pushData(topic, payload, unit):
  try:
    if enableMqtt != 1:
      return
    jsonObj = {'topic': topic, 'payload': payload, 'retain': 1, 'qos': 1}
    print("MQTT - Pushing with payload", jsonObj)
    response = post(haUrl, json=jsonObj, headers=headers)
    print("MQTT - server response", response)
  except:
    print("MQTT - Failed to push data")

# Initial OBDII data connection
try:
  initObd2Connection()
except:
  print("INIT - OBDII connection failed, will retry next loop")

# Loop to check for state of OBDII data connection
while True:
  try:
    # have some minimal err handling
    if initOk:
      print("MAIN - send Energica specific OBDII command")
      response = send(b"001\r")     # gets Energica specific data
      response = stripFormat(response)
      length = len(response)
      print("MAIN - received response", response, "with length", length)
      # only handle valid length responses, use 28 for non stripped, 19 for stripped
      if length >= 19: 
        handleStrippedResponse(response)
    else:
      print("MAIN - no OBDII conn. available")
      time.sleep(restartDelay)
      initObd2Connection()
  except:
    print("MAIN - failed getting valid OBDII response, delayed", restartDelay)
    # wait a bit longer and try full on reconnect
    time.sleep(restartDelay)
    initObd2Connection()
  print("MAIN - polling delayed", pollDelay)
  time.sleep(pollDelay)
