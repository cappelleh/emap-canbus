# README #

## What is this repository for? ##

Info collected on CAN bus communication with Zero and Energica Electric motorcycles using Bluetooth OBDII dongles based on `ELM237` chipset. 

For a simple to use Android App that connects to these Bluetooth OBD dongles including support for pushing data to the cloud and/or local home assistant configurations see this repo instead: 

https://bitbucket.org/cappelleh/emapp-android/

For an example python script that pushes data to a local Home assustant config, including examples of HA configuration files see the folder home-assistant in this repo. 

# OBDII general Info

## Hardware

For simple OBDII readouts an OBDII bluetooth dongle is all you need. For iOS it might be needed to get a WiFi enabled dongle instead. Note that not all OBDII apps have support for these wifi enabled dongles.

In general the sub 10 EUR dongles are all chines copies and often badly implemented and in some cases not even funtional. Best to stay away from these. 

Here is some info on those chinese copies https://timyouard.wordpress.com/2015/09/02/disection-of-a-counterfeit-elm327-obdii-adapter-from-china/
And this is some more info from the carscanner app https://www.carscanner.info/choosing-obdii-adapter/

You plug that into the OBDII port on your bike, pair your device with it and then you can select that bluetooth device from the OBD app settings you use.


## Protocol

General info on OBDII protocol but also great links to sniffing and other awesome resources can be found at https://www.csselectronics.com/screen/page/simple-intro-obd2-explained

An overview of OBDII standard PID values on this wiki https://en.wikipedia.org/wiki/OBD-II_PIDs#Service_01

And for conversion of HEX values you receive towards decimals see https://kb.iu.edu/d/afdl . Note that many of these values come with a formula in order to calculate it, see the previous overview on wikipedia. 


### An Example message explained

this is info from https://www.csselectronics.com/screen/page/simple-intro-obd2-explained

> An example of a request/response CAN message for the PID 'Vehicle Speed' with a value of 50 km/h can look like this:
> 
> Request: 7DF 02 01 0D 55 55 55 55 55
> Response: 7E8 03 41 0D 32 AA AA AA AA
> 
> Identifier: For OBD2 messages, the identifier is standard 11-bit and used to distinguish between “request messages” (ID 7DF) and “response messages” (ID 7E8 to 7EF). Note that 7E8 will typically be where the main engine or ECU responds at.
> Length: This simply reflects the length in number of bytes of the remaining data (03 to 06). For the Vehicle Speed example, it is 02 for the request (since only 01 and 0D follow), while for the response it is 03 as both 41, 0D and 32 follow.
> Mode: For requests, this will be between 01-0A. For responses the 0 is replaced by 4 (i.e. 41, 42, … , 4A). There are 10 modes as described in the SAE J1979 OBD2 standard. Mode 1 shows Current Data and is e.g. used for looking at real-time vehicle speed, RPM etc. Other modes are used to e.g. show or clear stored diagnostic trouble codes and show freeze frame data.
> PID: For each mode, a list of standard OBD2 PIDs exist - e.g. in Mode 01, PID 0D is Vehicle Speed. For the full list, check out the Wikipedia OBD2 PID overview. Each PID has a description and some have a specified min/max and conversion formula.
> The formula for speed is e.g. simply A, meaning that the A data byte (which is in HEX) is converted to decimal to get the km/h converted value (i.e. 32 becomes 50 km/h above). For e.g. RPM (PID 0C), the formula is (256*A + B) / 4.
> A, B, C, D: These are the data bytes in HEX, which need to be converted to decimal form before they are used in the PID formula calculations. Note that the last data byte (after Dh) is not used.


## Some common EV PIDs

Not many are documented at this point but these are 

```
9A 	154 	6 	Hybrid/EV Vehicle System Data, Battery, Voltage 
5B 	91 	1 	Hybrid battery pack remaining life (formula 100/255*A)
```

And I found some discussions at https://www.insideevsforum.com/community/index.php?threads/scangauge-ii-obdii-extended-pids-temperature.4435/ and http://www.mykiasoulev.com/forum/viewtopic.php?t=135&start=370

# Energica

Standard used by Energica is ISO 15765-4 CAN (11 Bit ID, 500 KBit)

ISO 15765-2: The ISO-TP standard describes the 'Transport Layer', i.e. how to send data packets exceeding 8 bytes via CAN bus. This standard is important as it forms the basis for Unified Diagnostic Services (UDS) communication, which relies on sending multiframe CAN data packets.

## Examples

All the data can be found in the log file in this repo to experiment with. The standard OBDII requests are all preceded with the following AT command. Note that this is ELM237 specific info and just filters the responses on specific messages. 

```
277	39.168070	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	23	Sent "ATCRA7EF\r"
280	39.193037	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	18	Rcvd "OK\r\r>"
```

break down

```
AT		// AT command
CRA		// set can RX filter
7EF		// 7EF responses
```

### VIN request

service 09 PID 02 is VIN request

```
281	39.231569	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0902\r"
284	39.261842	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7EF037F0933\r"

7EF     // receive data identifier
03      // data length
7F
09      // service 09
33

285	39.286874	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	33	Rcvd "7EF10144902015A4E4E\r"

7EF     // receive data identifier
10      // data length
14
49
02      // PID for service 09 PID 02 is VIN
01
5A
4E
4E

```

followed by my VIN in the response


### EV Battery life request

Service 01 PID 5B
5B 	91 	1 	Hybrid battery pack remaining life
formula 100/255*A


```
312	41.521761	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "015B\r"

315	41.587085	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7EF037F0133\r"
316	41.588481	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7EF03415BFF\r"
317	41.686086	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	15	Rcvd "\r>"
318	41.722038	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	21	Sent "ATCAF0\r"

7EF     // response identifier
03      // data length
7F      // 
01      // service ID
33

7EF     // response identifier
03      // data length
41      //
5B      // PID
FF      // 255 value for SOH 
```
100/255*255 = 100% converted with formula

### 12V controle module voltage

Service 01 PID 42
42 	66 	2 	Control module voltage
formula (256A+B)/1000

```
300	41.143037	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0142\r"
303	41.168783	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7EF037F0133\r"
304	41.188039	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	27	Rcvd "7EF044142310B\r"

7EF
03
7F
01  // service
33

7EF
04
41
42  // PID
31  // dec 49
0B
```

256*49/1000 = 12,544 V for 12V system

### Ambient Air temp

service 01 PID 46
46 	70 	1 	Ambient air temperature
formula A - 40

```
306	41.325943	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0146\r"
309	41.388297	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7EF037F0133\r"
310	41.388444	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7EF0341463F\r"

7EF
03
7F
01  // service 01
33

7EF
03
41
46  // PID
3F  // to DEC = 63
```

using formula 63-40 = 23C = OK

## Energica specific values

When I googled for that `ATCRA200` all I could find was this Kia E Soul EV topic http://www.mykiasoulev.com/forum/viewtopic.php?t=135&start=490 where `AT CRA 200` is also logged.  

```
326	41.845333	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	23	Sent "ATCRA200\r"
329	41.886801	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	18	Rcvd "OK\r\r>"
330	41.915828	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	18	Sent "001\r"
333	41.961850	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	35	Rcvd "200164764170C01FFFE\r\r>"
```

So first the RX filter is set to filter on `200` response and the dongle responds with `OK`.

```
326	41.845333	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	23	Sent "ATCRA200\r"
329	41.886801	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	18	Rcvd "OK\r\r>"
```

The data requested next is `001` and the response received is `200164764170C01FFFE`. 

```
330	41.915828	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	18	Sent "001\r"
333	41.961850	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	35	Rcvd "200164764170C01FFFE\r\r>"
```

Now if I link that back to the info I have from that readout is that the SOC was at 71 or 70 on the bike and the battery temp was at 23C.

```
200
16  // 22 in dec = low batt temp
47  // 71 in dec matches SOC
64  // 100 in dec = SOH
17  // 23 in dec = high batt temp
0C  // see next (bPackV in V)
01  // 0C01 is 3084 /10 = 308.4 V packV value
FF  // see next (bPackI in A)
FE  // FFFE is -2 /10 = -0.2 A packI value 
```

Matching BLE values

```
vehicleStatus.soc = rx_buffer[2];
vehicleStatus.bPackV = ((float) ((rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280))) / 10.0f;
vehicleStatus.bPackI = ((float) ((short) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280)))) / 10.0f;

```

The minimum required AT commands for getting responses for address filtered on 200 looks like this (including restart)

```
ATWS
ATH1
ATE0
ATCAF0
ATCRA200
```

Other Addresses tested that did generated responses on the bus so far are listed below. Note that the lower the number the higher the priority. 
The 200 are likely BMS related. For now I've only completed 200 itself, I would also expect some charge state and current and so on in that range. 
Everything lower I would expect ride controls like throttle and breaking. For 400 and 500 I haven't checked yet. 
Those not listed could still be valid but just silent at that time. 

```
100		00	00	00	00	00	00	10	01
101		3E	3C	04	04	4D	00	00	00
102		80	10	02	44	94	FF	11	00		// first 3 bytes change on using blinkers and beam, could be 12V
104		D2	98	91	00	00	00	00	00
109		00	00	64	00	B0	04	00	02		// first 2 bytes are throttle position with E8 03 when @ 100%

200		0F	4C	64	0F	0C	39	FF	FE		// see next chapter
201		01	00	00	00	00	00	00	00		// first digit is charge state, 01=IDLE, 02=AC, 16=DC charging
202		06	DC	00	E0	0F	A0	04	B0
203		0F	2D	56	29	0F	25	0F	35      // 2 last pairs are cell min and max voltage in mV unit, ex 0F35 = 3893 mV

400		1A	6F	0B	EA	00	02	00	80

500		01	00	00	00	00	00	00	00
501		AF	31	27	23	3C	0A	8E	0D
```

### Energica AC charging

Requesting above `001` response and filtering on `ATRCA200` while charging at around 3 kW gave some different responses so the charge (and ride) power are also there!

Hex values (except for header, first batt temp and SOC)

```
 A  B  C  D  E  F  G  H 
16 47 64 16 0C 17 00 5A
16 47 64 16 0C 17 00 5B
16 47 64 16 0C 17 00 5D
16 47 64 16 0C 17 00 5D
16 47 64 16 0C 17 00 5C
16 47 64 16 0C 17 00 5C
16 47 64 16 0C 17 00 5C
16 47 64 16 0C 17 00 5A
16 47 64 16 0C 17 00 5B
```

Decimal values for all Amp settings on AC charging that can be selected on the bike using the menu button (L and R). 

```
 G   H
000 005     01 A     0 kW    0.2 kW      0200
000 012     02 A     0 kW    0.3 kW      0300
000 017     03 A     0 kW    0.5 kW      0500
000 023     04 A     0 kW    0.8 kW      0800
000 031     05 A     0 kW    1.0 kW      1000
000 038     06 A     1 kW    1.2 kW      1200
000 045     07 A     1 kW    1.4 kW      1400
000 051     08 A     2 kW    1.6 kW      1600
000 061     09 A     2 kW    1.8 kW      1800
000 065     10 A     2 kW    2.0 kW      2000
000 071     11 A     2 kW    2.2 kW      2200
000 080     12 A     2 kW    2.4 kW      2400
000 085     13 A     2 kW    2.6 kW      2600
000 092     14 A     3 kW    2.8 kW      2800
```

The closest I got to a working formula is just H/6 = Amp value. 
Better formula is to work with the Amp value on DC level which is achieved with `25*G+H/10` 
and then convert from DC 330V to AC 220V (EU) system resulting in below formula:

```
(25*G+H/10)*330/220 = AC charging Amp value
(25*G+H/10)*330     = AC charging kW value
```

The G value was always 0 and the previous 2 values (E and F) just go up while charging (later on found that is battery Voltage).

From BLE connection I can see Energica is using MAINS voltage and current values 
to calculate AC charging power with below formula.

```
if (currentStatus.subState == 101) {
	chargePower = (int) (((double) currentStatus.cMAINSV) * 1.0d * ((double) currentStatus.cMAINSC));
}
```

TODO find MAINSC and MAINSV in OBD data

```
vehicleStatus.cMAINSV = (short) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280));
vehicleStatus.cMAINSC = ((float) ((short) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280)))) / 10.0f;
```

There is an option to limit AC charging power on the bike or using the BLE connection from your phone. 
Looks like on my bike this is currently set to 10A.

```
Max.cur (A)
10.0
100/10
0064
(?) 109 00 00 64 00 B0 04 00 04
(?) 447 40 30 0B 00 00 00 64 3A
```

From BLE

```
vehicleStatus.chgPowerLimit = rx_buffer[2];                    
```

### Energica DC charging

Some more DC charging values for different Amp settings. Looks like the G block is the K value, 
so as of 10kW that is a 001. I've since tested this up to 24 kW charge speed and it holds. 

```
 G   H
000 007     1 A 	<1 kW
000 047     5 A   	 1 kW
000 097    10 A   	 3 kW
000 147    15 A   	 5 kW
000 197    20 A   	 6 kW
000 248    25 A   	 8 kW
001 042    30 A  	10 kW
001 092    35 A  	11 kW
001 112    37 A  	12 kW
```

Working formula to get Amp values while DC charging is 25*G+H/10. For kW values use battery voltage (330) so (25*G+H/10)*330

```
(25*G+H/10)     = DC charging Amp value
(25*G+H/10)*330 = DC charging kW value
```

From the BLE connection I can see Energica is using bPackV and bPackI values to calculate DC charging power.

```
if (currentStatus.subState == 104) {
	chargePower = (int) (((double) currentStatus.bPackV) * 0.94d * ((double) currentStatus.bPackI));
}
```

From OBDII data

```
Pack V (V)
309.5
3095/10
0C17
(OK) 200 0D 49 64 0E 0C 17 FF FE

Pack I (A)
-0.2
-2/10
FFFE
(OK) 200 0D 49 64 0E 0C 17 FF FE
```

### Energica Power in kW while riding

I know it should be possible to retrieve power output (and input during regen) values while riding also.
I still need to work out how though. 

TODO check if last values of 200 address, known as bPackI value in Amps can be used to calcated current power? 
Knowing the bPackV or pack Voltage I would expect this to work similar on how the DC charge power is calculated. 

```
FFFE is -2 /10 = -0.2 A packI value 
```

From BLE connection I can see power while riding is calculated from motor rpm and motor torque instead. 

```
vehicleStatus.rpm = (rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280);
vehicleStatus.torque = (short) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280));
vehicleStatus.power = Math.round(((((float) (vehicleStatus.torque * 2)) * 3.1415927f) * ((float) vehicleStatus.rpm)) / 60000.0f);

```

### Energica Consumption Values

TODO There are also consumption values to be found, average and current consumption values. 
From BLE connection we have these:

```
vehicleStatus.avgConsumptionUnit = (char) (rx_buffer[6] & 255);
vehicleStatus.distanceUnit = (char) (rx_buffer[7] & 255);

vehicleStatus.range = (rx_buffer[5] & 255) | ((rx_buffer[6] << 8) & 65280);
vehicleStatus.instKmKwh = ((float) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280))) / 100.0f;
vehicleStatus.instKwh100Km = ((float) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280))) / 100.0f;
vehicleStatus.avgConsumption = ((float) ((((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280)) | ((rx_buffer[4] << 16) & 16711680)) | ((rx_buffer[5] << 24) & -16777216))) / 10.0f;

```

From OBDII Data
```
Avg.cons. (km/kWh)
327.67
32767/100
7FFF
(?) 102 80 10 02 44 99 FF 11 00
```

### Energica Battery State

There is cell balance we can use already at this point.

```
203     0F  2D  56  29  0F  25  0F  35      // 2 last pairs are cell min and max voltage in mV unit, ex 0F35 = 3893 mV
```

I would love to get the reserve battery value I see in BLE data directly from the bike. 

```
vehicleStatus.resBatteryEnergy = (short) ((rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280));
```

And an example from OBD data, was looking for `287C` here. Not found yet, 
could be that this is only updated when changing. So when charging or riding. 

```
Res.Energy (Wh)
Current value is dec 10364
hex 287C
```

TODO try again while charging

### Other Valid Energica Specific CAN responses

This list is the result from a quick scan I did from 100 to 899. At 800 it already rolls over to 000 
so 0 to 800 is the proper range. Some values don't show that often so in this form of scanning might not pop up. 

```
2022-02-06 09:24:22 +0000 Raw Rcd: 100 00 00 00 00 00 00 10 01
2022-02-06 09:24:27 +0000 Raw Rcd: 101 3E 3C 04 04 4B 00 00 00
2022-02-06 09:24:33 +0000 Raw Rcd: 102 80 10 02 44 99 FF 11 00
2022-02-06 09:24:45 +0000 Raw Rcd: 104 17 BF 01 00 00 00 00 00
2022-02-06 09:24:51 +0000 Raw Rcd: 105 06 01 00 01 00 00 EB 03
2022-02-06 09:25:33 +0000 Raw Rcd: 109 00 00 64 00 B0 04 06 04
2022-02-06 09:26:51 +0000 Raw Rcd: 122 06 00 00 00 00 00 00 00
2022-02-06 09:27:03 +0000 Raw Rcd: 124 FE EB 03 05 00 04 06 DE
2022-02-06 09:27:09 +0000 Raw Rcd: 125 00 00 00 00 00 00 00 00
2022-02-06 09:27:21 +0000 Raw Rcd: 127 00 00 00 00 01 33 00 00
2022-02-06 09:27:27 +0000 Raw Rcd: 128 00 00 00 00 00 00 00 00
2022-02-06 09:34:39 +0000 Raw Rcd: 200 0B 4B 64 0C 0C 50 00 51
2022-02-06 09:34:45 +0000 Raw Rcd: 201 02 00 00 00 00 00 08 00
2022-02-06 09:34:51 +0000 Raw Rcd: 202 D6 DC 00 E0 00 00 00 00
2022-02-06 09:34:57 +0000 Raw Rcd: 203 0F 4B 43 2E 0F 40 0F 58
2022-02-06 09:35:03 +0000 Raw Rcd: 203 0F 4B 43 2E 0F 40 0F 57
2022-02-06 09:35:21 +0000 Raw Rcd: 207 00 02 01 02 01 00 3D 01
2022-02-06 09:45:12 +0000 Raw Rcd: 305 02 89 00 59 00 87 0C 32
2022-02-06 09:45:19 +0000 Raw Rcd: 306 3D 2E CE 00 F4 0B B2
2022-02-06 09:45:25 +0000 Raw Rcd: 307 00 00 00
2022-02-06 09:48:41 +0000 Raw Rcd: 338 00 30 09 A0 4C 00 00 BC
2022-02-06 09:54:59 +0000 Raw Rcd: 400 1A 6F 0B EA 00 1C 00 80
2022-02-06 09:55:06 +0000 Raw Rcd: 400 07 6F 0B EA 00 1C 00 80
2022-02-06 09:56:02 +0000 Raw Rcd: 410 00 FF D9 05 CA 3B 00 00
2022-02-06 09:59:58 +0000 Raw Rcd: 447 40 30 0B 00 00 00 64 3A
2022-02-06 10:03:36 +0000 Raw Rcd: 480 04 00 17 F3 38 82 02 00
2022-02-06 10:05:52 +0000 Raw Rcd: 500 01 00 00 00 00 00 00 00
2022-02-06 10:05:59 +0000 Raw Rcd: 501 B7 31 0B 23 B0 09 3B 0D
2022-02-06 10:21:48 +0000 Raw Rcd: 625 68 01 4B FF 00 00 00 00
2022-02-06 10:32:15 +0000 Raw Rcd: 687 00 00 00 00 00 00 05 00
2022-02-06 11:02:10 +0000 Raw Rcd: 020 B3 00 B2 00 B2 00 C3 00
2022-02-06 11:02:33 +0000 Raw Rcd: 022 F0 D8 F0 D8 5B 00 00 00
2022-02-06 11:03:07 +0000 Raw Rcd: 025 24 0C 00 00 00 00 B1 FA
2022-02-06 11:03:30 +0000 Raw Rcd: 027 1A 00 00 00 01 00 06 00
```

To find some more values this how some are parsed from the Bluetooth connection with the bike. 
Other bluetooth values parsed within BLE connection shows the units handling.

```
vehicleStatus.state = rx_buffer[3];
vehicleStatus.subState = rx_buffer[4];

vehicleStatus.cDCC = ((float) ((short) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280)))) / 100.0f;
vehicleStatus.cDCV = ((float) ((short) ((rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280)))) / 10.0f;
vehicleStatus.speed = (rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280);

vehicleStatus.tripMeter = (float) ((rx_buffer[2] & 255) | (((rx_buffer[3] & 255) << 8) & 65280) | (((rx_buffer[4] & 255) << 16) & 16711680) | (((rx_buffer[5] & 255) << 24) & -16777216));
vehicleStatus.totalOdometer = (float) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280) | ((rx_buffer[4] << 16) & 16711680) | ((rx_buffer[5] << 24) & -16777216));
```

Attempt to match some more

```
Max.reg cur (A)
120.0
1200/10
04B0

12Volt (V)
12,73
1273/100
04F9
```

# Zero SR/F and SR/S

## AT command for 12V voltage

```
3490	7065.437913	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "ATRV\r"
3493	7065.476928	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	21	Rcvd "13.3V\r\r>"
```

Not really an OBDII standard but just and `elm327` AT command that retrieves the 12V value (as a string) from the OBD port itself. 
The OBDII standard value for this would be Service 01 PID 42 (see earlier).


## Actual Data Examples OBDIII standard compliant

There is a log file for the bluetooth commands from Zero also in this repo now. It was taken with the bike showing these values

```
OBD Module voltage 13.3 V
DTC count 1
Distance traveled since codes cleared 13686 km
Ambient air temperature 21 C
Fuel Type Electric
Hybrid/EV battery pack remaining charge 68 %
Vehicle Odometer Reading 136861.5 km
```

## Examples

### service 09 PID 02 VIN request

```
3388	7061.560820	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0902\r"
3391	7061.603181	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	51	Rcvd "7E81014490201353338\r7E8215A46415A37344"
3392	7061.630537	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	35	Rcvd "C\r7E822434B3132323932\r"
```

### service 09 PID 0A ECU name

```
3399	7061.838928	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "090A\r"
3402	7061.883067	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	51	Rcvd "7E81014490A015A4552\r7E8214F4D41494E424"
3403	7061.905601	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	35	Rcvd "9\r7E8224B45424F415244\r"
```

### service 01 PID 0C engine speed (rpm)

```
3499	7065.585675	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "010C\r"
3502	7065.627115	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	27	Rcvd "7E804410C0000\r"
```

### service 01 PID 0D vehicle speed

```
3504	7065.687445	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "010D\r"
3507	7065.726941	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E803410D00\r"
```

### service 01 PID 11 throttle position

```
3509	7065.800486	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0111\r"
3512	7065.853445	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E803411100\r"
```

### service 01 PID 1F runtime since engine start

```
3514	7065.892866	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "011F\r"
3517	7065.931726	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	27	Rcvd "7E804411F0000\r"
```

### service 01 PID 21 distance traveled with malfunction lamp (MIL) on

```
3519	7065.984981	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0121\r"
3522	7066.027031	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	27	Rcvd "7E80441210000\r"
```

### service 01 PID 31 distance traveled since codes cleared

```
3524	7066.084081	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0131\r"
3527	7066.126943	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	27	Rcvd "7E80441313576\r"
```

### service 01 PID 46 ambient air temperature

```
3529	7066.189658	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0146\r"
3532	7066.231994	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E80341463D\r"
```
formula A - 40

```
7E8
03
41
46	// PID
3D	// 61
```

61 - 40 = 21 C

### service 01 PID 49 accelerator pedal position D

```
3534	7066.280352	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0149\r"
3537	7066.326788	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E803414900\r"
```

### service 01 PID 51 Fuel Type (value 8 is electric)

```
3539	7066.382326	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0151\r"
3542	7066.439206	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E803415108\r"
```

formula 8 is electric

```
7E8
03
41
51	// PID
08	// electric
```


### service 01 PID 5B hybrid battery pack remaining life

```
3544	7066.487566	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "015B\r"
3547	7066.526930	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E803415BAF\r"
```
formula is 100/255*A

```
7E8
03
41
5B	// PID
AF	// in hex is 175 
```

100/255*175 = 68,6274509804 = 68 % SOC (not SOH)

### service 01 PID 61 drivers demand engine - percent torque

```
3549	7066.586696	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0161\r"
3552	7066.627133	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E80341617D\r"
```

### service 01 PID 62 actual engine - percent torque

```
3554	7066.686622	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "0162\r"
3557	7066.726771	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	25	Rcvd "7E80341627D\r"
```

### service 01 PID 7F engine run time

```
3559	7066.787964	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "017F\r"
3562	7066.833431	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	55	Rcvd "7E8100F417F00000000\r7E82100000000000000\r7E"
3563	7066.834279	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	31	Rcvd "82200000000000000\r"
```

### service 01 PID A6 odometer

```
3565	7066.896848	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "01A6\r"
3568	7066.933227	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	31	Rcvd "7E80641A60014E227\r"
```

formula is (A*2^24 + B*2^16 + C*2^8 + D) / 10

```
7E8
06
41
A6		// PID
00		// A
14		// B
E2		// C
27		// D
```

(00*2^24 + 20*2^16 + 226*2^8 + 39) / 10 = 136.861,5 = 13.686,15 km

### service 01 PID 9A Hybrid/EV Vehicle System Data, Battery, Voltage 

```
3570	7066.987824	LGElectr_2a:bf:68 (Nexus 5X)	66:1e:11:00:f1:43 (OBDII)	SPP	19	Sent "019A\r"
3573	7067.028028	66:1e:11:00:f1:43 (OBDII)	LGElectr_2a:bf:68 (Nexus 5X)	SPP	53	Rcvd "7E81008419A00000000\r7E82100000000000000\r"
```

We get a value back but I have no way to validate it to be honest. 

```
7E8		// identifier
10		// length? from hex to dec is 16
08		// mode? 
41		// ? from hex to dec is 65...
9A		// PID
00000000

7E8
21		// ? from hex to dec is 33...
00000000000000

```

Bike was in idle state, I'll get a new response in riding and charging mode just to check for more values here. 

